## Website Boilerplate

Made for Anita and Kimmie

A quick create-react-app website boiler plate for anyone to be able to edit and build their own websites.

UI has not been implemented, but components have been added so all a user needs to do is to use the existing components and import images.

